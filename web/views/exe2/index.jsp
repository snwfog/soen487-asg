<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <h1>Exercise 2 - Temperature Conversion</h1>

    <p>If you see this message, then the web service has started correctly. The web service is launched on port 12345
        and the wsdl is located at <a href="http://localhost:12345/temp?wsdl">localhost:12345</a>.</p>

    <div class="well">
        <form class="form-horizontal" action="/exe2" method="post">
            <legend>Temperature Conversion</legend>
            <div class="control-group">
                <div class="controls">
                    <input id="temperature" name="temperature" type="text" placeholder="Example: 23.0"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="convert-to" id="to-celsius" value="to-celsius" checked/>
                        Convert from Fahrenheit to Celsius
                    </label>
                    <label class="radio">
                        <input type="radio" name="convert-to" id="to-fahrenheit" value="to-fahrenheit" checked/>
                        Convert from Celsius to Fahrenheit
                    </label>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-info">Convert</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>
