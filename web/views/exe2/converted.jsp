<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <h2>
        <% String unit = ""; %>
        Converted Temperature:
        <% if ( request.getAttribute( "convertedTo" ).equals( "to-celsius" ) ) { %>
            From Fahrenheit to Celsius
            <% unit = "Celsius"; %>
        <% } else if ( request.getAttribute( "convertedTo" ).equals( "to-fahrenheit" ) ) { %>
            From Celsius to Fahrenheit
            <% unit = "Fahrenheit"; %>
        <% }%>
    </h2>

    <div class="well">
        <h1><%= request.getAttribute( "convertedTemperature" ) %> <%= unit %></h1>
    </div>
</div>

</body>
</html>
