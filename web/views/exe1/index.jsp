<%@ page import="java.util.Calendar" %>
<%@ page import="org.w3c.dom.NodeList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.w3c.dom.Node" %>
<%@ page import="org.w3c.dom.Element" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">

    <% NodeList nodeList = (NodeList) request.getAttribute( "nodeList" ); %>
    <h1>Le Devoir (<%= (new SimpleDateFormat("EEE, d MMM yyyy")).format( new Date()) %>)</h1>

    <div class="alert">
        <ul>
            <li>The servlet will fetch the xml and parse it and display all the articles title into the page</li>
            <li>The links will take the viewer to the corresponding page</li>
            <li>The servlet is synchronous and runs in the same thread</li>
        </ul>
    </div>

    <ul>
        <% for ( int i = 0; i < nodeList.getLength(); i++ ) { %>
            <% Node n = nodeList.item( i ); %>
            <% Element e = (Element) n; %>
            <li>
                <h2>
                    <a href="<%= e.getElementsByTagName("link").item(0).getTextContent() %>">
                        <%= e.getElementsByTagName( "title" ).item( 0 ).getTextContent() %>
                    </a>
                </h2>
            </li>
        <% } %>
    </ul>
</div>

</body>
</html>
