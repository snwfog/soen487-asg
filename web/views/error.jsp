<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" isErrorPage="true" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Something went terribly wrong...</h1>

<p><%= exception.getMessage() %></p>

</body>
</html>
