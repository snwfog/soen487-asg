<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <h1>SOEN487 - Assignment 1</h1>

    <h2>Chao Yang - 5682061</h2>

    <div class="alert">
        <p>Readme</p>
        <ul>
            <li>I have not use Netbeans to develop the web service because it was generating a lot of codes that are not clean, and hard to understand, instead, I've chosen to go with the simplistic approach, and wrote all my web service manually using Java. You can deploy this assignment as a regular web app using Tomcat, Jetty, or Glassfish</li>
            <li>I have not test this on Netbeans, but it should be working</li>
            <li>Except for exercise 3 where the Top-down approach, the code is auto-generated from the WSDL</li>
            <li>This assignment is ran and deployed as a regular web application</li>
            <li>The web service endpoint will be created when you access exercise 2, 3, 4</li>
            <li>They use the localhost port 12345, 12346, 12347, 12348 respectively</li>
            <li>On every exercise, you will find the link to wsdl of each of the web service</li>
            <li>I have tested everything and works properly..</li>
        </ul>
    </div>

    <ul>
        <li><h2><a href="/exe1">Exercise 1</a></h2></li>
        <li><p>Will fetch the information from <a href="http://www.ledevoir.com/rss/edition_complete.xml">Le Devoir</a>
            XML feeds. It might take a while depending on your current internet connection.</p></li>

        <li><h2><a href="/exe2">Exercise 2</a></h2></li>
        <li><p>Will create a localhost web service and give a form to call the web service.</p></li>

        <li><h2><a href="/exe3">Exercise 3</a></h2></li>
        <li><p>Will create a localhost web service and give a form to call the web service.</p></li>

        <li><h2><a href="/exe4">Exercise 4</a></h2></li>
        <li><p>Will create a localhost web service and give a form to call the web service.</p></li>
    </ul>

    <%@ include file="/views/footer.jsp" %>

</div>
</body>
</html>
