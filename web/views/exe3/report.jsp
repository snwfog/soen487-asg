<%@ page import="asg.exe4.model.Product" %>
<%@ page import="asg.exe3.CreditReport" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<% CreditReport r = (CreditReport) request.getAttribute( "creditReport" ); %>

<div class="container-fluid">
    <h1><%= r.getFirstName() %>, <%= r.getLastName() %></h1>
    <h3><%= r.getDob() %></h3>
    <h3><%= r.getSsn() %></h3>
    <h3><%= r.getScore() %></h3>
    <h3><%= r.getLatestAddress1() %></h3>
    <h3><%= r.getLatestAddress2() %></h3>
    <h3><%= r.getCity() %></h3>
    <h3><%= r.getState() %></h3>
    <h3><%= r.getCountry() %></h3>
    <h3><%= r.getPostalCode() %></h3>
    <h3><%= r.getLiability().toString() %></h3>
    <h3><%= r.getLiquidAssests().toString() %></h3>
    <h3><%= r.getImmovableAssests().toString() %></h3>
    <h3><%= r.getCurrency() %></h3>
</div>

</body>
</html>

