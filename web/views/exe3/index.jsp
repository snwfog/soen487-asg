<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <h1>Exercise 3 - Credit Report</h1>

    <div class="alert">If you see this message, then the web service has started correctly. The web service is launched
        on port 12345 and the wsdl is located at <a href="http://localhost:12348/credit?wsdl">localhost:12348</a>.</div>

    <div class="well">
        <form class="form-horizontal" action="/exe3" method="post">
            <legend>Report Query</legend>
            <div class="control-group">
                <div class="controls">
                    <input id="first-name" name="first-name" type="text" placeholder="Example: Charles"
                           value="Charles" />
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <input id="last-name" name="last-name" type="text" placeholder="Example: Yang" value="Yang" />
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input id="ssn" name="ssn" type="text" placeholder="Example: 123 456 789" value="123 456 789" />
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input id="query-type" name="query-type" type="text" placeholder="Example: Search"
                           value="Display"/>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-info">Query</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>
