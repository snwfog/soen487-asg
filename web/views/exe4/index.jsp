<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <h1>Supply Chain Management</h1>
    <div class="alert">
        <p>Readme</p>
        <ul>
            <li>The manufacturer and warehouse web service are connected</li>
            <li>Manufacturer will let user create order and will be produced by manufacturer in batches</li>
            <li>The get product info will let user get information about a type of product</li>
            <li>Receive payment will let user pay for an order</li>
            <li>All orders will show all the orders</li>
            <li>All information are stored into xml files for persistence</li>
            <li>Warehouse will let user ship to a customer</li>
            <li>When shipping to a customer a back ordered product, it will automatically call manufacturer service to order new products if the product inventory does not exists anymore</li>
            <li>When an inventory is shipped to a customer, it will have <code>isShip</code> marked as <code>true</code>, and the customer information will be attached with the inventory</li>
            <li>Check inventory will list all the current inventory</li>
        </ul>
    </div>
    <h1>Manufacturer Web Service</h1>

    <p>If you see this message, then the web service has started correctly. The web service is launched on port 12346
        and the wsdl is located at <a href="http://localhost:12346/manufacturer?wsdl">localhost:12346</a>.</p>
    <p>Choose from one of the following options: </p>
    <ul>
        <li><a href="/exe4/create">Create Order</a></li>
        <li><a href="/exe4/info">Get Product Info</a></li>
        <li><a href="/exe4/payment">Receive Payment (Require Order ID and the BOM)</a></li>
        <li><a href="/exe4/orders">All Orders</a></li>
    </ul>

    <h1>Warehouse Web Service</h1>

    <p>If you see this message, then the web service has started correctly. The web service is launched on port 12347
        and the wsdl is located at <a href="http://localhost:12347/warehouse?wsdl">localhost:12347</a>.</p>
    <p>Choose from one of the following options: </p>
    <ul>
        <li><a href="/exe4/warehouse/ship">Ship Inventory</a></li>
        <li><a href="/exe4/warehouse/inventory">Check Inventory</a></li>
    </ul>

</div>

</body>
</html>
