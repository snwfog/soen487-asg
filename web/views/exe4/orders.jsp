<%@ page import="java.util.Calendar" %>
<%@ page import="org.w3c.dom.NodeList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.w3c.dom.Node" %>
<%@ page import="asg.exe4.model.PurchaseOrder" %>
<%@ page import="asg.exe4.model.PurchaseOrders" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <% PurchaseOrders po = (PurchaseOrders) request.getAttribute( "purchaseOrders" ); %>
    <h1>Purchase Orders</h1>
    <ul>
        <% for (PurchaseOrder order : po.getPurchaseOrders() ) { %>
        <li>
            <h2>Purchase Order ID: <%= order.getOrderId() %></h2>
            <h4>Customer Reference: <%= order.getCustomRef() %></h4>
            <h4>Product: <%= order.getProduct() %></h4>
            <h4>Product Unit Price: <%= order.getUnitPrice() %></h4>
            <h4>Quantity: <%= order.getQuantity() %></h4>
            <h4>BOM: $<%= order.getQuantity() * order.getUnitPrice() %></h4>
            <h4>Is Paid: <%= order.getIsPaid() ? "Yes" : "No" %></h4>
        </li>
        <% } %>
    </ul>
</div>

</body>
</html>
