<%@ page import="asg.exe4.model.Product" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<% Product p = (Product) request.getAttribute( "product" ); %>

<div class="container-fluid">
    <h1><%= p.getProductType() %></h1>
    <h3>Manufacturer: <%= p.getManufacturerName() %></h3>
    <h3>Manufacture Unit Price: <%= p.getUnitPrice() %></h3>
</div>

</body>
</html>
