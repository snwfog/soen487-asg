<%@ page import="asg.exe4.model.Inventories" %>
<%@ page import="asg.exe4.model.Inventory" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <% Inventories inventories = (Inventories) request.getAttribute( "inventories" ); %>
    <h1>Inventories</h1>
    <ul>
        <% for ( Inventory inv : inventories.getInventorys() ) { %>
        <li>
            <h2>Product: <%= inv.getProduct() %>
            </h2>
            <h4>Customer: <%= inv.getCustomer() %>
            </h4>
            <h4>Is Shipped: <%= inv.getIsShipped() %>
            </h4>
        </li>
        <% } %>
    </ul>
</div>

</body>
</html>
