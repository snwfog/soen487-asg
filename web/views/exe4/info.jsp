<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <h1>Manufacturer Web Service</h1>

    <p>If you see this message, then the web service has started correctly. The web service is launched on port 12346
        and the wsdl is located at <a href="http://localhost:12346/manufacturer?wsdl">localhost:12346</a>.</p>

    <div class="well">
        <form class="form-horizontal" action="/exe4/info" method="post">
            <legend>Product Info</legend>
            <div class="control-group">
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="product-type" id="dvd" value="dvd" checked/>DVD Player
                    </label>
                    <label class="radio">
                        <input type="radio" name="product-type" id="tv" value="tv" checked/>TV
                    </label>
                    <label class="radio">
                        <input type="radio" name="product-type" id="camera" value="camera" checked/>Video Camera
                    </label>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-info">Get Info</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>
