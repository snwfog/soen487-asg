<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chao Yang - 5682061 - SOEN487 - Assignment 1</title>
    <link rel="stylesheet" href="/views/bootplus.min.css" type="text/css">
    <link rel="stylesheet" href="/views/bootplus-responsive.min.css" type="text/css">
</head>
<body>

<div class="container-fluid">
    <h1>Warehouse Web Service</h1>

    <p>If you see this message, then the web service has started correctly. The web service is launched on port 12347
        and the wsdl is located at <a href="http://localhost:12347/warehouse?wsdl">localhost:12347</a>.</p>

    <div class="well">
        <form class="form-horizontal" action="/exe4/warehouse/ship" method="post">
            <legend>Ship Order</legend>
            <div class="control-group">
                <div class="controls">
                    <input id="customer-reference" name="reference" type="text" placeholder="Customer: 923223"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input id="customer-name" name="name" type="text" placeholder="Name: Charles Yang"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input id="customer-address" name="address" type="text"
                           placeholder="Address: 123 St-Marc Montreal"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="type" id="dvd" value="dvd" checked/>DVD Player
                    </label>
                    <label class="radio">
                        <input type="radio" name="type" id="tv" value="tv" checked/>TV
                    </label>
                    <label class="radio">
                        <input type="radio" name="type" id="camera" value="camera" checked/>Video Camera
                    </label>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-info">Ship Order</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>
