package asg;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/5/2014
 * Time:   8:43 PM
 *
 */
public class TemperatureServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        System.out.println( "Starting a servlet on port 12345" );
        Endpoint.publish( "http://localhost:12345/temp", new TemperatureConversionImpl() );

        req.getRequestDispatcher( "/views/exe2/index.jsp" ).forward( req, resp );
    }

    @Override
    protected void doPost( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        try {
            double temperature = Double.parseDouble( req.getParameter( "temperature" ) );
            String convertTo = req.getParameter( "convert-to" );

            URL url = new URL( "http://localhost:12345/temp?wsdl" );
            QName qName = new QName( "http://asg/", "TemperatureConversionImplService" );
            Service service = Service.create( url, qName );
            TemperatureConversion tempService = service.getPort( TemperatureConversion.class );

            if ( convertTo.equals( "to-celsius" ) ) {
                req.setAttribute( "convertedTemperature", tempService.toCelsius( temperature ) );
            } else if ( convertTo.equals( "to-fahrenheit" ) ) {
                req.setAttribute( "convertedTemperature", tempService.toFahrenheit( temperature ) );
            }

            req.setAttribute( "convertedTo", convertTo );
            req.getRequestDispatcher( "/views/exe2/converted.jsp" ).forward( req, resp );
        } catch ( NumberFormatException e ) {
            // Forward to error servlet
        }
    }
}
