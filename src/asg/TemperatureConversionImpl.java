package asg;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/6/2014
 * Time:   1:42 PM
 *
 */
@WebService(endpointInterface = "asg.TemperatureConversion")
public class TemperatureConversionImpl implements TemperatureConversion {

    @WebMethod
    public double toCelsius( double fahrenheit ) {
        return ((fahrenheit - 32) / 9) * 5;
    }

    @WebMethod
    public double toFahrenheit( double celsius ) {
        return (celsius / 5) * 9 + 32;
    }
}
