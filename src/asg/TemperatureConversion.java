package asg;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/5/2014
 * Time:   7:57 PM
 *
 */
@WebService
public interface TemperatureConversion {
    @WebMethod
    public double toCelsius( double fahrenheit );
    @WebMethod
    public double toFahrenheit( double celsius );
}
