package asg;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class LeDevoirServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
        URL url = new URL( "http://www.ledevoir.com/rss/edition_complete.xml" );
        System.out.println( "Fetching from " + url.toString() );
        URLConnection connection = url.openConnection();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xmlNews = builder.parse( connection.getInputStream() );

            NodeList nodeList = xmlNews.getElementsByTagName( "item" );
            req.setAttribute( "nodeList", nodeList );
            req.getRequestDispatcher( "/views/exe1/index.jsp" ).forward( req, resp );

        } catch ( Exception e ) {

        }
    }
}
