package asg.exe3;

import java.math.BigInteger;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   6:50 AM
 *
 */
@WebService(endpointInterface = "asg.exe3.CreditReportPortType")
public class CreditReportPortTypeImpl implements CreditReportPortType {

    @WebMethod
    public CreditReport creditReportOperation( CreditQuery part1 ) {
        CreditReport report = (new ObjectFactory()).createCreditReport();

        report.setFirstName( "Charles" );
        report.setLastName( "Yang" );
        report.setDob( "September 7, 1985" );
        report.setSsn( "123 456 789" );
        report.setScore( "9000" );
        report.setLatestAddress1( "123 St-Marc" );
        report.setLatestAddress2( "456 Patricia" );
        report.setCity( "Montreal" );
        report.setState( "Quebec" );
        report.setCountry( "Canada" );
        report.setPostalCode( "H3H 2P1" );
        report.setLiability( new BigInteger( "123123123123123123123123" ) );
        report.setLiquidAssests( new BigInteger( "456456456456456456456456" ) );
        report.setImmovableAssests( new BigInteger( "789789789789789789789789" ) );
        report.setCurrency( "AUS" );

        return report;
    }
}
