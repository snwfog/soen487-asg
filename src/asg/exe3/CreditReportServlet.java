package asg.exe3;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import asg.exe4.IWSManufacturer;
import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrder;
import asg.exe4.model.PurchaseOrders;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   6:45 AM
 *
 */
public class CreditReportServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        System.out.println( "Starting a servlet on port 12346 & 12347" );
        Endpoint.publish( "http://localhost:12348/credit", new CreditReportPortTypeImpl() );
        req.getRequestDispatcher( "/views/exe3/index.jsp" ).forward( req, resp );
    }

    @Override
    protected void doPost( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        try {

            String firstName = req.getParameter( "first-name" );
            String lastName = req.getParameter( "last-name" );
            String ssn = req.getParameter( "ssn" );
            String queryType = req.getParameter( "query-type" );

            CreditQuery cq = (new ObjectFactory()).createCreditQuery();
            cq.setFirstName( firstName );
            cq.setLastName( lastName );
            cq.setSsn( ssn );
            cq.setQueryType( queryType );

            URL url = new URL( "http://localhost:12348/credit?wsdl" );
            QName qName = new QName( "http://exe3.asg/", "CreditReportPortTypeImplService" );
            Service service = Service.create( url, qName );
            CreditReportPortType creditReportService = service.getPort( CreditReportPortType.class );
            CreditReport report = creditReportService.creditReportOperation( cq );

            req.setAttribute( "creditReport", report );
            req.getRequestDispatcher( "/views/exe3/report.jsp" ).forward( req, resp );
        } catch ( NumberFormatException e ) {
            // Forward to error servlet
        }
    }
}
