package asg.exe4.manufacturer;

import java.io.File;

import asg.exe4.model.Product;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   8:03 PM
 *
 */
public class TVManufacturer extends Manufacturer {

    public TVManufacturer( File database ) {
        super( database );
    }

    @Override
    public boolean produce( Product.ProductType type, int quantity ) {
        return false;
    }
}
