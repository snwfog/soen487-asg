package asg.exe4.manufacturer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import asg.exe4.model.Product;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   7:56 PM
 *
 */
public abstract class Manufacturer {

    private File database;

    public Manufacturer( File database ) {
        this.database = database;
    }

    public boolean produce( Product.ProductType type, int quantity ) {
        return (type.equals( Product.ProductType.DVD_PLAYER )
                || type.equals( Product.ProductType.VIDEO_CAMERA )
                || type.equals( Product.ProductType.TV ));
    }

    public static Manufacturer getManufacturer( Product.ProductType type ) {
        Manufacturer manufacturer;

        if ( type.equals( Product.ProductType.DVD_PLAYER ) ) {
            manufacturer = new DVDManufacturer( new File( "." ) );
        } else if ( type.equals( Product.ProductType.TV ) ) {
            manufacturer = new TVManufacturer( new File( "." ) );
        } else {
            manufacturer = new VideoCameraManufacturer( new File( "." ) );
        }

        return manufacturer;
    }

    public void writeToFile( Product product ) throws FileNotFoundException, JAXBException {
        //FileOutputStream fis = new FileOutputStream( database, true );
        JAXBContext context = JAXBContext.newInstance( Product.class );
        Marshaller m = context.createMarshaller();
        m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
        m.marshal( product, System.out );
    }
}

