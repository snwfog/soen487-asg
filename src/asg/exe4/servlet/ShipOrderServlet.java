package asg.exe4.servlet;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import asg.exe4.IWSManufacturer;
import asg.exe4.IWSWarehouse;
import asg.exe4.WSWarehouse;
import asg.exe4.model.Customer;
import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrder;
import asg.exe4.model.PurchaseOrders;
import asg.exe4.model.Warehouse;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   3:33 AM
 *
 */
public class ShipOrderServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        req.getRequestDispatcher( "/views/exe4/ship.jsp" ).forward( req, resp );
    }

    @Override
    protected void doPost( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        try {

            String customerReference = req.getParameter( "reference" );
            String customerName = req.getParameter( "name" );
            String customerAddress = req.getParameter( "address" );
            String productType = req.getParameter( "type" );

            Product p = Product.dvdPlayerInstance();
            if (productType.equals("dvd"))
                p = Product.dvdPlayerInstance();
            else if (productType.equals("tv"))
                p = Product.tvInstance();
            else
                p = Product.videoCameraInstance();

            Customer customer = new Customer();
            customer.setReference( customerReference );
            customer.setName( customerName );
            customer.setAddress( customerAddress );

            URL url = new URL( "http://localhost:12347/warehouse?wsdl" );
            QName qName = new QName( "http://exe4.asg/", "WSWarehouseService" );
            Service service = Service.create( url, qName );
            IWSWarehouse warehouseService = service.getPort( IWSWarehouse.class );
            boolean shipped = warehouseService.shipGood( p.getProductType(), customer );
            System.out.println("Is the good shipped? " + shipped);

            resp.sendRedirect( "/exe4" );
        } catch ( NumberFormatException e ) {
            // Forward to error servlet
        }
    }
}
