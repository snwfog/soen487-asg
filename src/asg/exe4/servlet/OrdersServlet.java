package asg.exe4.servlet;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrders;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   11:20 PM
 *
 */
public class OrdersServlet extends HttpServlet {
    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        try {
            req.setAttribute( "purchaseOrders", PurchaseOrders.getInstance() );
        } catch ( JAXBException e ) {
            e.printStackTrace();
        } catch ( URISyntaxException e ) {
            e.printStackTrace();
        }
        req.getRequestDispatcher( "/views/exe4/orders.jsp" ).forward( req, resp );
    }
}
