package asg.exe4.servlet;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import asg.exe4.IWSManufacturer;
import asg.exe4.WSManufacturer;
import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrder;
import asg.exe4.model.PurchaseOrders;
import sun.net.httpserver.HttpServerImpl;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   8:57 PM
 *
 */
public class CreateOrderServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        req.getRequestDispatcher( "/views/exe4/create.jsp" ).forward( req, resp );
    }

    @Override
    protected void doPost( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        try {

            int quantity = Integer.parseInt( req.getParameter( "quantity" ) );
            float unitPrice = Float.parseFloat( req.getParameter( "unit-price" ) );
            String customerReference = req.getParameter( "reference" );
            String productType = req.getParameter( "type" );
            Product p = Product.dvdPlayerInstance();
            if (productType.equals("dvd"))
                p = Product.dvdPlayerInstance();
            else if (productType.equals("tv"))
                p = Product.tvInstance();
            else
                p = Product.videoCameraInstance();

            PurchaseOrder po = new PurchaseOrder();
            po.setQuantity( quantity );
            po.setUnitPrice( unitPrice );
            po.setProduct( p );
            po.setCustomRef( customerReference );


            URL url = new URL( "http://localhost:12346/manufacturer?wsdl" );
            QName qName = new QName( "http://exe4.asg/", "WSManufacturerService" );
            Service service = Service.create( url, qName );
            IWSManufacturer manufacturerService = service.getPort( IWSManufacturer.class );
            manufacturerService.processOrder( po );

            req.setAttribute( "purchaseOrders", PurchaseOrders.getInstance() );
            req.getRequestDispatcher( "/views/exe4/orders.jsp" ).forward( req, resp );
        } catch ( NumberFormatException e ) {
            // Forward to error servlet
        } catch ( URISyntaxException e ) {
            e.printStackTrace();
        } catch ( JAXBException e ) {
            e.printStackTrace();
        }
    }
}
