package asg.exe4.servlet;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import asg.exe4.IWSWarehouse;
import asg.exe4.model.Inventories;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   3:45 AM
 *
 */
public class InventoryServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        URL url = new URL( "http://localhost:12347/warehouse?wsdl" );
        QName qName = new QName( "http://exe4.asg/", "WSWarehouseService" );
        Service service = Service.create( url, qName );
        IWSWarehouse warehouseService = service.getPort( IWSWarehouse.class );
        Inventories inv = warehouseService.checkInventories();

        req.setAttribute( "inventories", inv );
        req.getRequestDispatcher( "/views/exe4/inventories.jsp" ).forward( req, resp );
    }
}
