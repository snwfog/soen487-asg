package asg.exe4.servlet;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import asg.exe4.IWSManufacturer;
import asg.exe4.model.Product;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   8:57 PM
 *
 */
public class ProductionInfoServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
        req.getRequestDispatcher( "/views/exe4/info.jsp" ).forward( req, resp );
    }

    @Override
    protected void doPost( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
        try {
            String type = req.getParameter( "product-type" );

            URL url = new URL( "http://localhost:12346/manufacturer?wsdl" );
            QName qName = new QName( "http://exe4.asg/", "WSManufacturerService" );
            Service service = Service.create( url, qName );
            IWSManufacturer manufacturerService = service.getPort( IWSManufacturer.class );

            Product.ProductType productType = Product.ProductType.DVD_PLAYER;
            switch ( type ) {
                case "tv":
                    productType = Product.ProductType.TV;
                    break;
                case "camera":
                    productType = Product.ProductType.VIDEO_CAMERA;
                    break;
                default:
                    productType = Product.ProductType.DVD_PLAYER;
            }

            Product product = manufacturerService.getProductInfo( productType );
            req.setAttribute( "product", product );
            req.getRequestDispatcher( "/views/exe4/product.jsp" ).forward( req, resp );
        } catch ( NumberFormatException e ) {
            // Forward to error servlet
        }
    }
}
