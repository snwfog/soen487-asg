package asg.exe4.servlet;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import asg.TemperatureConversion;
import asg.TemperatureConversionImpl;
import asg.exe4.WSManufacturer;
import asg.exe4.WSWarehouse;
import sun.net.httpserver.HttpServerImpl;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   8:58 PM
 *
 */
public class ManufacturerWSServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp )
            throws ServletException, IOException {
        System.out.println( "Starting a servlet on port 12346 & 12347" );
        Endpoint.publish( "http://localhost:12347/warehouse", new WSWarehouse() );
        Endpoint.publish( "http://localhost:12346/manufacturer", new WSManufacturer() );

        req.getRequestDispatcher( "/views/exe4/index.jsp" ).forward( req, resp );
    }
}
