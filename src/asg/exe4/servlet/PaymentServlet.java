package asg.exe4.servlet;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import asg.exe4.IWSManufacturer;
import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrders;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   8:57 PM
 *
 */
public class PaymentServlet extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
        req.getRequestDispatcher( "/views/exe4/payment.jsp" ).forward( req, resp );
    }

    @Override
    protected void doPost( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
        try {
            long orderId = Long.parseLong( req.getParameter( "order-id" ) );
            float totalPrice = Float.parseFloat(req.getParameter( "order-total-price" ) );

            URL url = new URL( "http://localhost:12346/manufacturer?wsdl" );
            QName qName = new QName( "http://exe4.asg/", "WSManufacturerService" );
            Service service = Service.create( url, qName );
            IWSManufacturer manufacturerService = service.getPort( IWSManufacturer.class );

            manufacturerService.receivePayment( orderId, totalPrice );

            req.setAttribute( "purchaseOrders", PurchaseOrders.getInstance() );
            req.getRequestDispatcher( "/views/exe4/orders.jsp" ).forward( req, resp );
        } catch ( NumberFormatException e ) {
            // Forward to error servlet
        } catch ( URISyntaxException e ) {
            e.printStackTrace();
        } catch ( JAXBException e ) {
            e.printStackTrace();
        }
    }
}
