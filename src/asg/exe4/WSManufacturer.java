package asg.exe4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import javax.xml.ws.Endpoint;

import asg.exe4.manufacturer.Manufacturer;
import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrder;
import asg.exe4.model.PurchaseOrders;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   3:02 PM
 *
 */
@WebService(endpointInterface = "asg.exe4.IWSManufacturer")
public class WSManufacturer implements IWSManufacturer {

    @WebMethod
    public void processOrder( PurchaseOrder order ) {
        if ( order.getUnitPrice() >= order.getProduct().getUnitPrice() ) {
            Product.ProductType type = order.getProduct().getProductType();
            Manufacturer manufacturer = Manufacturer.getManufacturer( type );
            manufacturer.produce( type, order.getQuantity() );

            try {

                PurchaseOrders po = PurchaseOrders.getInstance();
                po.getPurchaseOrders().add( order );
                po.save();

            } catch ( JAXBException e ) {
                e.printStackTrace();
            } catch ( FileNotFoundException e ) {
                e.printStackTrace();
            } catch ( URISyntaxException e ) {
                e.printStackTrace();
            }
        }
    }

    @WebMethod
    public Product getProductInfo( Product.ProductType productType ) {
        return Product.getInfo( productType );
    }

    @Override
    public boolean receivePayment( long orderId, float bom ) {
        try {
            PurchaseOrder purchaseOrder = new PurchaseOrder();
            purchaseOrder.setOrderId( orderId );

            PurchaseOrders po = PurchaseOrders.getInstance();
            for (PurchaseOrder p : po.getPurchaseOrders())
            {
                if (purchaseOrder.equals(p))
                {
                    p.setIsPaid( true );
                    po.save();
                    return true;
                }
            }
        } catch ( JAXBException e ) {
            e.printStackTrace();
        } catch ( URISyntaxException e ) {
            e.printStackTrace();
        } catch ( FileNotFoundException e ) {
            e.printStackTrace();
        }

        return false;
    }

    public static void main( String[] args ) {
        Endpoint.publish( "http://localhost:12345/manufacturer", new WSManufacturer() );
    }

}
