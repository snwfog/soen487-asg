package asg.exe4;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import asg.exe4.model.Customer;
import asg.exe4.model.Inventories;
import asg.exe4.model.Product;
import asg.exe4.model.Warehouse;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   3:05 AM
 *
 */
@WebService(endpointInterface = "asg.exe4.IWSWarehouse")
public class WSWarehouse implements IWSWarehouse {
    @WebMethod
    public boolean shipGood( Product.ProductType type, Customer customer ) {
        Warehouse w = null;
        boolean shipped = false;
        try {
            w = Warehouse.getInstance();
            shipped =  w.shipProduct( type, customer );
            if (!shipped)
            {
                w.replenish( type );
                shipped = w.shipProduct( type, customer );
            }
        } catch ( FileNotFoundException e ) {
            e.printStackTrace();
        } catch ( JAXBException e ) {
            e.printStackTrace();
        } catch ( URISyntaxException e ) {
            e.printStackTrace();
        } catch ( MalformedURLException e ) {
            e.printStackTrace();
        }

        return shipped;
    }

    @WebMethod
    public Inventories checkInventories()
    {
        Warehouse w = null;
        try {
            w = Warehouse.getInstance();
        } catch ( FileNotFoundException e ) {
            e.printStackTrace();
        } catch ( JAXBException e ) {
            e.printStackTrace();
        } catch ( URISyntaxException e ) {
            e.printStackTrace();
        }

        return w.getInventories();
    }
}
