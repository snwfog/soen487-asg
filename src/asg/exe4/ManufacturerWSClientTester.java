package asg.exe4;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import asg.exe4.WSManufacturer;
import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrder;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   8:23 PM
 *
 */
public class ManufacturerWSClientTester {
    public static void main( String[] args ) throws MalformedURLException {
        URL url = new URL( "http://localhost:12345/manufacturer?wsdl" );
        QName qName = new QName( "http://exe4.asg/", "WSManufacturerService" );
        Service service = Service.create( url, qName );
        IWSManufacturer manufacturerService = service.getPort( IWSManufacturer.class );

        Product p = new Product();
        p.setManufacturerName( "Apple" );
        p.setProductType( Product.ProductType.DVD_PLAYER );
        p.setUnitPrice( 123.99f );

        PurchaseOrder order = new PurchaseOrder();
        order.setUnitPrice( 239.99f );
        order.setCustomRef( "IPOD" );
        order.setOrderId( 12345 );
        order.setProduct( p );
        order.setQuantity( 1000 );
        manufacturerService.processOrder( order );
        //Product prod = manufacturerService.getProductInfo( Product.ProductType.DVD_PLAYER );
    }
}
