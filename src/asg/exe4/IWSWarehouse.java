package asg.exe4;

import javax.jws.WebMethod;
import javax.jws.WebService;

import asg.exe4.model.Customer;
import asg.exe4.model.Inventories;
import asg.exe4.model.Product;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   2:30 AM
 *
 */
@WebService
public interface IWSWarehouse {
    @WebMethod
    public boolean shipGood(Product.ProductType type, Customer customer);

    @WebMethod
    public Inventories checkInventories();
}
