package asg.exe4;

import javax.jws.WebMethod;
import javax.jws.WebService;

import asg.exe4.model.Product;
import asg.exe4.model.PurchaseOrder;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   8:32 PM
 *
 */
@WebService
public interface IWSManufacturer {

    @WebMethod
    public void processOrder( PurchaseOrder order );

    @WebMethod
    public Product getProductInfo( Product.ProductType productType );

    @WebMethod
    public boolean receivePayment( long orderId, float bom );
}
