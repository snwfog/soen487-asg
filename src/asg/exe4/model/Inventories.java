package asg.exe4.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   3:20 AM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "inventories")
public class Inventories {

    @XmlElement(name = "inventory", type = Inventory.class)
    private List<Inventory> inventorys = new ArrayList<>();

    public Inventories() { }

    public Inventories( List<Inventory> inventories ) {
        this.inventorys = inventories;
    }

    public List<Inventory> getInventorys() {
        return inventorys;
    }

    public void setInventorys( List<Inventory> inventories ) {
        this.inventorys = inventories;
    }

    public static Inventories getInstance() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( Inventories.class );
        Unmarshaller um = context.createUnmarshaller();
        URL fileUrl = Inventories.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/inventorydb.xml" );
        File file = new File( fileUrl.toURI() );
        return (Inventories) um.unmarshal( new FileInputStream( file ) );
    }

    public void save() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( Inventories.class );
        Marshaller m = context.createMarshaller();
        URL fileUrl = Inventories.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/inventorydb.xml" );
        File file = new File( fileUrl.toURI() );
        m.marshal( this, new FileOutputStream( file ) );
    }

    public static void main( String[] args ) throws JAXBException, UnsupportedEncodingException {
        JAXBContext context = JAXBContext.newInstance( Inventories.class );
        Unmarshaller um = context.createUnmarshaller();
        Marshaller m = context.createMarshaller();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        m.marshal( new Inventories(), os );
        String prod = new String( os.toByteArray() );
        Inventories inv = (Inventories) um.unmarshal( new ByteArrayInputStream( prod.getBytes( "UTF-8" ) ) );
    }

}
