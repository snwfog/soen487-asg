package asg.exe4.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   10:49 PM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "purchaseOrders")
public class PurchaseOrders {

    @XmlElement(name = "purchaseOrder", type = PurchaseOrder.class)
    private List<PurchaseOrder> purchaseOrders = new ArrayList<>();

    public PurchaseOrders() {
    }

    public PurchaseOrders( List<PurchaseOrder> purchaseOrders ) {
        this.purchaseOrders = purchaseOrders;
    }

    public List<PurchaseOrder> getPurchaseOrders() {
        return this.purchaseOrders;
    }

    public void setPurchaseOrders( List<PurchaseOrder> orders ) {
        this.purchaseOrders = orders;
    }

    public static PurchaseOrders getInstance() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( PurchaseOrders.class );
        Unmarshaller um = context.createUnmarshaller();
        URL fileUrl = PurchaseOrder.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/orderdb.xml" );
        File file = new File( fileUrl.toURI() );
        FileInputStream inputStream = new FileInputStream( file );

        //Marshaller m = context.createMarshaller();
        //ByteArrayOutputStream os = new ByteArrayOutputStream();
        //m.marshal( new PurchaseOrders(), os );
        //String prod = new String( os.toByteArray() );
        //ByteArrayInputStream inputStream = new ByteArrayInputStream( prod.getBytes() );

        return (PurchaseOrders) um.unmarshal( inputStream );
    }

    public void save() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( PurchaseOrders.class );
        Marshaller m = context.createMarshaller();
        URL fileUrl = PurchaseOrder.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/orderdb.xml" );
        File file = new File( fileUrl.toURI() );
        m.marshal( this, new FileOutputStream( file ) );
    }

    public static void main( String[] args ) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance( PurchaseOrders.class );
        Unmarshaller um = context.createUnmarshaller();
        Marshaller m = context.createMarshaller();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        m.marshal( new PurchaseOrders(), os );
        String prod = new String( os.toByteArray() );
        PurchaseOrders po = (PurchaseOrders) um.unmarshal( new ByteArrayInputStream( prod.getBytes() ) );
    }

}
