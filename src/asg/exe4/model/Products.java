package asg.exe4.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   10:49 PM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "products")
public class Products {

    @XmlElement(name = "product", type = Product.class)
    private List<Product> products = new ArrayList<>();

    public Products() {
    }

    public Products( List<Product> productList ) {
        this.products = productList;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts( List<Product> products ) {
        this.products = products;
    }

    public static Products getInstance() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( Products.class );
        Unmarshaller um = context.createUnmarshaller();
        URL fileUrl = Products.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/productdb.xml" );
        File file = new File( fileUrl.toURI() );
        return (Products) um.unmarshal( new FileInputStream( file ) );
    }

    public void save() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( PurchaseOrders.class );
        Marshaller m = context.createMarshaller();
        URL fileUrl = Products.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/productdb.xml" );
        File file = new File( fileUrl.toURI() );
        m.marshal( this, new FileOutputStream( file ) );
    }

}
