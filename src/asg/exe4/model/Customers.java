package asg.exe4.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   3:09 AM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "customers")
public class Customers {

    @XmlElement(name = "customer", type = Customer.class)
    private List<Customer> customers = new ArrayList<>();

    public Customers() {
    }

    public Customers( List<Customer> customers ) {
        this.customers = customers;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers( List<Customer> customers ) {
        this.customers = customers;
    }

    public static Customers getInstance() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( Customers.class );
        Unmarshaller um = context.createUnmarshaller();
        URL fileUrl = Customers.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/customerdb.xml" );
        File file = new File( fileUrl.toURI() );
        return (Customers) um.unmarshal( new FileInputStream( file ) );
    }

    public void save() throws JAXBException, URISyntaxException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance( Customers.class );
        Marshaller m = context.createMarshaller();
        URL fileUrl = Customers.class.getClassLoader().getResource( "./asg/exe4/model/xmldb/customerdb.xml" );
        File file = new File( fileUrl.toURI() );
        m.marshal( this, new FileOutputStream( file ) );
    }

    public static void main( String[] args )
            throws JAXBException, UnsupportedEncodingException, FileNotFoundException, URISyntaxException {
        JAXBContext context = JAXBContext.newInstance( Customers.class );
        Unmarshaller um = context.createUnmarshaller();
        Marshaller m = context.createMarshaller();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        m.marshal( new Customers(), os );
        String prod = new String( os.toByteArray() );
        Customers customers = (Customers) um.unmarshal( new ByteArrayInputStream( prod.getBytes( "UTF-8" ) ) );
        customers.save();
    }
}
