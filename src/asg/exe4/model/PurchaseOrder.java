package asg.exe4.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   2:59 PM
 *
 */
@XmlRootElement
@XmlType(propOrder = { "orderId", "customRef", "product", "quantity", "unitPrice", "isPaid" })
public class PurchaseOrder {

    private static long purchaseId = 1;
    private long orderId;
    private String customRef;
    private Product product;
    private int quantity;
    private float unitPrice;
    private boolean isPaid;

    public PurchaseOrder() {
        orderId = purchaseId++;
        isPaid = false;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId( long orderNum ) {
        this.orderId = orderNum;
    }

    public String getCustomRef() {
        return customRef;
    }

    public void setCustomRef( String customRef ) {
        this.customRef = customRef;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct( Product product ) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity( int quantity ) {
        this.quantity = quantity;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice( float unitPrice ) {
        this.unitPrice = unitPrice;
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid( boolean isPaid ) {
        this.isPaid = isPaid;
    }

    @Override
    public boolean equals(Object po)
    {
        if (po == null) return false;
        if (this == po) return true;
        if (!(this instanceof PurchaseOrder)) return false;
        PurchaseOrder p = (PurchaseOrder) po;

        return this.orderId == p.orderId
                && Math.abs(this.quantity * this.unitPrice) - (p.quantity * p.unitPrice) < 0.01;
    }

    public static void main( String[] args ) throws JAXBException, UnsupportedEncodingException {
        JAXBContext context = JAXBContext.newInstance( PurchaseOrder.class );
        Unmarshaller um = context.createUnmarshaller();
        Marshaller m = context.createMarshaller();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        m.marshal( new PurchaseOrder(), os );
        String prod = new String( os.toByteArray() );
        PurchaseOrder po = (PurchaseOrder) um.unmarshal( new ByteArrayInputStream( prod.getBytes( "UTF-8" ) ) );

    }
}
