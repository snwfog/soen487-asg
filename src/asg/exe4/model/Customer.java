package asg.exe4.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   2:32 AM
 *
 */
@XmlRootElement
@XmlType(propOrder = { "reference", "name", "address" })
public class Customer {
    private String reference;
    private String name;
    private String address;

    public String getReference() {
        return reference;
    }

    public void setReference( String reference ) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress( String address ) {
        this.address = address;
    }

    @Override
    public String toString()
    {
        return name + " - " + reference + " - " + address;
    }

    public static void main( String[] args ) throws JAXBException, UnsupportedEncodingException {
        JAXBContext context = JAXBContext.newInstance( Customer.class );
        Unmarshaller um = context.createUnmarshaller();
        Marshaller m = context.createMarshaller();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        m.marshal( new Customer(), os );
        String prod = new String( os.toByteArray() );
        Customer customer = (Customer) um.unmarshal( new ByteArrayInputStream( prod.getBytes( "UTF-8" ) ) );
    }
}
