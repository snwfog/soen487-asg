package asg.exe4.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/11/2014
 * Time:   3:00 PM
 *
 */
@XmlRootElement
@XmlType(propOrder = { "manufacturerName", "productType", "unitPrice" })
public class Product {

    // FIXME: Move these as file stream
    public static Product dvdPlayerInstance() {
        Product p = new Product();
        p.setManufacturerName( "Apple" );
        p.setProductType( ProductType.DVD_PLAYER );
        p.setUnitPrice( 123.99f );
        return p;
    }

    public static Product videoCameraInstance() {
        Product p = new Product();
        p.setManufacturerName( "Hasselblad" );
        p.setProductType( ProductType.VIDEO_CAMERA );
        p.setUnitPrice( 899.99f );
        return p;
    }

    public static Product tvInstance() {
        Product p = new Product();
        p.setManufacturerName( "Samsung" );
        p.setProductType( ProductType.TV );
        p.setUnitPrice( 1999.99f );
        return p;
    }

    public static Product getNewInstance( ProductType type ) {
        switch ( type ) {
            case DVD_PLAYER:
                return dvdPlayerInstance();
            case VIDEO_CAMERA:
                return videoCameraInstance();
            default:
                return tvInstance();
        }
    }

    public static enum ProductType {
        DVD_PLAYER,
        VIDEO_CAMERA,
        TV
    }

    private String manufacturerName;
    private ProductType productType;
    private float unitPrice;

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice( float unitPrice ) {
        this.unitPrice = unitPrice;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType( ProductType productType ) {
        this.productType = productType;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName( String manufacturerName ) {
        this.manufacturerName = manufacturerName;
    }

    @Override
    public String toString() {
        return this.getProductType() + " - " + this.getManufacturerName() + " - " + this.getUnitPrice();
    }

    public static Product getInfo( ProductType type ) {
        switch ( type ) {
            case DVD_PLAYER:
                return dvdPlayerInstance();
            case TV:
                return tvInstance();
            case VIDEO_CAMERA:
                return videoCameraInstance();
        }

        return new Product();
    }
}
