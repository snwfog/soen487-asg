package asg.exe4.model;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import asg.exe4.IWSManufacturer;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   2:39 AM
 *
 */
public class Warehouse {

    private int replenishStep = 50;
    private String customerReferenceNumber = "123123";

    private Inventories inventories;
    private Customers customers;

    private static Warehouse instance;

    public Warehouse() throws FileNotFoundException, JAXBException, URISyntaxException {
        inventories = Inventories.getInstance();
        customers = Customers.getInstance();
    }

    public static Warehouse getInstance() throws FileNotFoundException, JAXBException, URISyntaxException {
        if ( instance == null ) {
            instance = new Warehouse();
        }
        return instance;
    }

    public Inventories getInventories() {
        return inventories;
    }

    public boolean shipProduct( Product.ProductType type, Customer customer ) {
        for ( Inventory inv : inventories.getInventorys() ) {
            if ( inv.getProduct().getProductType() == type && !inv.getIsShipped() ) {
                inv.setIsShipped( true );
                inv.setCustomer( customer );

                try {
                    customers.getCustomers().add( customer );
                    customers.save();
                    inventories.save();
                } catch ( JAXBException e ) {
                    e.printStackTrace();
                } catch ( URISyntaxException e ) {
                    e.printStackTrace();
                } catch ( FileNotFoundException e ) {
                    e.printStackTrace();
                }
                return true;
            }
        }

        return false;
    }

    public void replenish( Product.ProductType type ) throws MalformedURLException {
        URL url = new URL( "http://localhost:12346/manufacturer?wsdl" );
        QName qName = new QName( "http://exe4.asg/", "WSManufacturerService" );
        Service service = Service.create( url, qName );
        IWSManufacturer manufacturerService = service.getPort( IWSManufacturer.class );
        // Get the product type
        Product product = manufacturerService.getProductInfo( type );
        float willingToPay = product.getUnitPrice() * 1.02f; // Pay only 2% more

        PurchaseOrder po = new PurchaseOrder();
        po.setQuantity( replenishStep );
        po.setUnitPrice( willingToPay );
        po.setProduct( product );
        po.setCustomRef( this.customerReferenceNumber );

        manufacturerService.processOrder( po );

        // FIXME: Immediately replenish...
        for ( int i = 0; i < replenishStep; i++ ) {
            this.getInventories().getInventorys().add( new Inventory( Product.getNewInstance( type ) ) );
        }
    }
}
