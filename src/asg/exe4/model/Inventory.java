package asg.exe4.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: Charles Chao Yang (http://github.com/snwfog)
 * Date:   2/12/2014
 * Time:   3:16 AM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "inventory")
//@XmlType(propOrder = { "product", "isShipped", "customer" })
public class Inventory {

    @XmlElement(name = "product", type = Product.class)
    private Product product;

    @XmlElement(name = "isShipped", type = boolean.class)
    private boolean isShipped;

    @XmlElement(name = "customer", type = Customer.class)
    private Customer customer;

    public Inventory() {}

    public Inventory(Product p)
    {
        this.product = p;
        this.isShipped = false;
        this.customer = null;
    }


    public Product getProduct() {
        return product;
    }

    public void setProduct( Product product ) {
        this.product = product;
    }

    public boolean getIsShipped() {
        return isShipped;
    }

    public void setIsShipped( boolean isShipped ) {
        this.isShipped = isShipped;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer( Customer customer ) {
        this.customer = customer;
    }

}
